# hsDownloads

is an advanced Downloads module for your Drupal 8.x site. 

With this powerful module, you can easily set a _downloads section on your 
site_ and offer unlimited amount of files to your users. 


### Features

  * supports subcategories
  * integrated permissions system
  * a complete notifications system
